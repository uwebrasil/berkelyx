# BerkeleyX: CS169.1x Engineering Software as a Service
# HOMEWORK PART 2
# Uwe Kristmann
# 04.05.2014

def hello(name)
    "Hello, "+name
end

# Define a method starts_with_consonant?(s) that takes a string and returns true 
# if it starts with a consonant and false otherwise. 
# (For our purposes, a consonant is any letter other than A, E, I, O, U.)
# NOTE: be sure it works for both upper and lower case and for nonletters! 
def starts_with_consonant?(s)  
   if s.empty?
      return false
   end 
   if s.downcase[0].ord <97 or s.downcase[0].ord > 122
      return false
   end 
   b = "aeiou".include?(s.downcase[0])
   !b  
end

# Define a method binary_multiple_of_4?(s) that takes a string 
# and returns true if the string represents a binary number that is a multiple of 4. 
# NOTE: be sure it returns false if the string is not a valid binary number! 
def binary_multiple_of_4?(s)
    #if (s =~ /^[0-1]{1,}$/) == nil
    #  return false
    #end
    if s=="0"
       return true
    end
    if s.match(/^[0-1]{1,}$/) == nil
       return false
    end 
    if s.match(/^[10]*00$/)
       return true
    end
    false
end

