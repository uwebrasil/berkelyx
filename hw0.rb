#BerkeleyX: CS169.1x Engineering Software as a Service
# HOMEWORK PART 1
# Uwe Kristmann
# 04.05.2014

# returns array-sum, 0 if empty array
def sum(a)
    if a.empty? 
        return 0
    end
    raise "all entries <array> must be integers" unless a.all? { |i| i.is_a? Fixnum }    
    a.inject{|sum,x| sum + x} 
end

# returns sum of the two largest element
# 0 if array empty
# element if array only has one element
def max_2_sum(a)
    if a.empty? 
        return 0
    end
    if a.count == 1
      return a[0]
    end    
    raise "all entries <array> must be integers" unless a.all? { |i| i.is_a? Fixnum }        
    sum(a.sort.reverse.take(2))
end

# Define a method sum_to_n? which takes an array of integers and an additional integer, n, as arguments and 
# returns true if any two distinct elements in the array of integers sum to n. 
# An empty array or single element array should both return false. 
def sum_to_n?(a,n)
    if a.empty? 
        return false
    end
    if a.count == 1
      return false
    end    
    raise "all entries <array> must be integers" unless a.all? { |i| i.is_a? Fixnum } 
    raise "<n> must be an integer" unless n.kind_of? Integer
    b = a.combination(2).to_a   
    c = []
    b.each {|x|c.push(sum(x))}    
    c.include?(n)    
end  
